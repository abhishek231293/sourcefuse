const express = require('express');
const router = express.Router();
const TradesController = require('../controllers/trades')

module.exports = router;

router.post("/", TradesController.addTrades)
router.get("/", TradesController.getTrades)
router.get("/:trade_id", TradesController.getTrade)
router.all("/:trade_id", TradesController.notAllowed)
