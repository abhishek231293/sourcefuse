const Trade = require('../models/trades');

class TradesController {

    //Add New Trades
    static addTrades = async(req, res) => {
        try {

            if (!req.body) {
                res.send({
                    status_code: 400,
                    message: 'Paramter Missing',
                    data: []
                });
            }

            const { type, user_id, symbol, shares, price } = req.body

            const tradeDetail = Trade.build({
                "type": type,
                "user_id": user_id,
                "symbol": symbol,
                "shares": shares,
                "price": price
            });

            const newTrade = await tradeDetail.save()

            res.send({
                status_code: 201,
                message: 'Trade added successfully!',
                data: newTrade
            });

        } catch(error) {
            let errorList = [];
            error.errors.forEach((err)=>{
                errorList.push(err.message);
            })

            res.send({
                status_code: 400,
                message: errorList,
                data: []
            });
        }
    }

    //Get All Trades
    static getTrades = async(req, res) => {
        try {

            const { type, user_id } = req.query;

            let where = [];

            if(type){
                where.push({type})
            }
            if(user_id){
                where.push({user_id})
            }

            let tradesList = await Trade.findAll({ where, raw: true });
            res.send({
                status_code: 200,
                message: 'Trades list fetched successfully!',
                data: tradesList
            });

        } catch(error) {
            let errorList = [];
            error.errors.forEach((err)=>{
                errorList.push(err.message);
            })

            res.send({
                status_code: 400,
                message: errorList,
                data: []
            });
        }
    }

    //Get Trade By Trade Id
    static getTrade = async(req, res) => {
        try {

            const { trade_id } = req.params;

            let tradeDetail = await Trade.findOne({
                                                    raw: true,
                                                    where: {  id : trade_id }
                                                });
            res.send({
                status_code: tradeDetail?200:404,
                message: tradeDetail?'Trade detail fetched successfully!':'Id not found',
                data: tradeDetail ? tradeDetail : []
            });

        } catch(error) {
            let errorList = [];
            error.errors.forEach((err)=>{
                errorList.push(err.message);
            })

            res.send({
                status_code: 400,
                message: errorList,
                data: []
            });
        }
    }

    //Restricting other request methods
    static notAllowed = async(req, res) => {
        res.send({
            status_code: 405,
            message: "Method Not Allowed",
            data: []
        });
    }

}

module.exports = TradesController
