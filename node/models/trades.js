const {Sequelize} = require("sequelize");
const sequelize = new Sequelize("sqlite::memory:");

// Insert your model definition here
const Trades = sequelize.define('Trades', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    type: {
        type: Sequelize.ENUM,
        values: ['buy', 'sell'],
        allowNull: false,
        validate: {
            isIn: {
                args: [['buy', 'sell']],
                msg: "Type must be Buy or Sell"
            }
        }
    },
    user_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        validate: {
            isNumeric: {
                args: true,
                msg: "User Id must be an Integer"
            }
        }
    },
    symbol: {
        type: Sequelize.STRING,
        allowNull: false
    },
    shares: {
        type: Sequelize.INTEGER,
        allowNull: false,
        validate: {
            min: {
                args: 1,
                msg: "Share must be greater than 0"
            },
            max:  {
                args: 100,
                msg: "Share must be less than or equal 100"
            },
            isNumeric: {
                args: true,
                msg: "Share must be an integer value"
            }
        },
    },
    price: {
        type: Sequelize.INTEGER,
        allowNull: false,
        validate: {
            isNumeric: {
                args: true,
                msg: "Price must be an integer value"
            }
        }
    }
}, {
    freezeTableName: true
});

module.exports = Trades
