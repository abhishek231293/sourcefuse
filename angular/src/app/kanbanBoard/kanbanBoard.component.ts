import {Component, OnInit} from '@angular/core';
import { Task } from "./task.interface";
import { TaskService } from "./task.service";

@Component({
  selector: 'kanban-board',
  templateUrl: './kanbanBoard.component.html',
  styleUrls: ['./kanbanBoard.component.scss']
})
export class KanbanBoard implements OnInit {
  newTask: Task = {name: '', stage: 0};
  tasks: Task[];
  stagesNames;
  error: string = '';
  stagesTasks: any[];

  constructor(private taskService: TaskService) {
  }

  ngOnInit() {

    this.taskService.getTaskList().subscribe((value) => {
      this.tasks = value;
    });

    this.taskService.getStageList().subscribe((value) => {
      this.stagesNames = value;
    });

    this.configureTasksForRendering();
  }

  configureTasksForRendering = () => {
    this.stagesTasks = [];
    for (let i = 0; i < this.stagesNames.length; ++i) {
      this.stagesTasks.push([]);
    }
    for (let task of this.tasks) {
      const stageId = task.stage;
      this.stagesTasks[stageId].push(task);
    }
  }

  addNewTask() {

    if(this.isValid()){
      this.taskService.addNewTask(this.newTask);
      this.configureTasksForRendering();
      this.resetNewTaskForm();
    }

  }

  existingTask() {
    return this.tasks.find(task => task.name.toLowerCase() === this.newTask.name.trim().toLowerCase());
  }

  isValid() {
    this.error = '';

    if(!this.newTask.name.trim()){
      this.error = 'Task name required!';
      return false;
    }

    const task = this.existingTask();
    if(task) {
      this.error = 'Task already exist!';
      return false;
    }

    return true;
  }

  changeStage(selectedTask, direction) {

    this.tasks = this.taskService.taskUpdate(selectedTask, direction);
    this.configureTasksForRendering();

  }

  removeTask(selectedTask) {
    this.tasks = this.taskService.deleteTask(selectedTask);
    this.configureTasksForRendering();
  }

  resetNewTaskForm () {
    this.newTask = {name: '', stage: 0};
  }

  generateTestId = (name) => {
    return name.split(' ').join('-');
  }
}
