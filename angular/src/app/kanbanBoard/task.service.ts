import { Injectable } from '@angular/core';
import {Task} from "./task.interface";
import {Observable, of} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  tasks: Array<Task> = [
    { name: 'Task 1', stage: 0 },
    { name: 'Task 2', stage: 2 }
  ];

  stagesNames: Array<String> = ['Backlog', 'To Do', 'Ongoing', 'Done'];

  constructor() {
  }

  getTaskList(): Observable<Task[]> {
    return of(this.tasks);
  }

  getStageList(): Observable<String[]> {
    return of(this.stagesNames);
  }

  addNewTask(task: Task) {
    task.name = task.name.trim();
    this.tasks.push(task);
  }

  taskUpdate(selectedTask: Task, direction: string) {
    this.tasks = this.tasks.map(function (task) {
      if(task.name === selectedTask.name) {
        if(direction == 'backward') {
          task.stage = task.stage - 1;
        }else{
          task.stage = task.stage + 1;
        }
      }
      return task;
    });

    return this.tasks
  }

  deleteTask(selectedTask) {
    this.tasks = this.tasks.filter(task => task.name !== selectedTask.name);

    return this.tasks;
  }
}
